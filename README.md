# bit_tree_lite

Bittree is a simple bit-vector enhanced with  successor queries that work in time log_64(N). In other words you can quickly get the next element present in the bit vector. 

The successor queries  can be used to quickly clear the whole data structure, in time proportional to the number of present elements (and not in N).
For practical sizes of N, the remaining operations (insert, erase, membership test) add only a small overhead over.

A comprehensive description is underway...