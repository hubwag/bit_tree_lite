/*  Copyright 2013-2019 IST Austria
    Author: Hubert Wagner (hub.wag@gmail.com)

    This file is part of BITTREE_LITE.

    BITTREE_LITE is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BITTREE_LITE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with BITTREE_LITE.  If not, see <http://www.gnu.org/licenses/>. 
    */


#include <vector>
#include <cstdint>

class bit_indexer64 {
  size_t index_of_2_to_power_i[64];
  const size_t sequence = 0x07EDD5E59A4E28C2ULL;
public:
  bit_indexer64() {
    for (size_t i = 0; i < 64; i++)
      index_of_2_to_power_i[(sequence << i) >> 58] = i;
  }
  size_t rightmost_pos(const uint64_t value) const {
    return index_of_2_to_power_i[((value & (-(int64_t)value)) * sequence) >> 58];
  }
};

class bit_tree {
protected:
  using block_type = uint64_t;
  const size_t block_size_in_bits = 64;
  const size_t block_shift = 6;
  const block_type ONE = 1; // 1<<n may be the wrong type.
                            // Some compilers failed to optimize multiplication by block_size_in_bits.    
  const block_type block_modulo_mask = (ONE << block_shift) - 1;

  size_t offset; // data[i + offset] = ith block of the data-bitset    
  std::vector<block_type> data;
  bit_indexer64 indexer;

  // Used to mask out bits to the 'right' (inclusive) from a given index.
  // Note that shifting a w-bit word by w bits is not well defined.
  block_type ones_from_left_to_index_ex(size_t index) const {
    return (index + 1 != block_size_in_bits) * ((~block_type(0)) << ((index + 1)));
  }
public:
  void init(size_t num_elements) {
    int64_t n = 1;
    int64_t bottom_blocks_needed = (num_elements + block_size_in_bits - 1) / block_size_in_bits;
    int64_t upper_blocks = 1;

    // How many blocks are needed to summarize the whole structure?
    while (n * block_size_in_bits < bottom_blocks_needed) {
      n *= block_size_in_bits;
      upper_blocks += n;
    }
    offset = upper_blocks;
    data.resize(upper_blocks + bottom_blocks_needed, 0);
  }
  bool is_empty() const {
    return data[0] == 0;
  }
  void xor_index(const size_t entry) {
    size_t index_in_level = entry >> block_shift;
    size_t address = index_in_level + offset;
    size_t index_in_block = entry & block_modulo_mask;
    block_type mask = ONE << index_in_block;

    data[address] ^= mask;
    // We go up. We stop when we reached the root,
    // or if anyone else is in the block.
    while (address && !(data[address] & ~mask)) {
      index_in_block = index_in_level & block_modulo_mask;
      index_in_level >>= block_shift;
      address = (address - 1) >> block_shift;
      mask = ONE << index_in_block;
      data[address] ^= mask;
    }
  }
  bool member(size_t entry) const {
    size_t index_in_level = entry >> block_shift;
    size_t address = index_in_level + offset;
    size_t index_in_block = entry & block_modulo_mask;
    block_type mask = ONE << (index_in_block);
    return data[address] & mask;
  }
  void insert(size_t entry) {
    if (!member(entry))
      xor_index(entry);
  }
  void erase(size_t entry) {
    if (member(entry))
      xor_index(entry);
  }
  size_t successor(size_t entry) const {
    size_t curr_block_in_level = entry >> block_shift; // base level
    size_t curr_block = curr_block_in_level + offset;
    size_t curr_bit_in_block = entry & block_modulo_mask;
    size_t curr_rightmost_mask = ones_from_left_to_index_ex(curr_bit_in_block);

    // Go up, and check if there is a block with something to the 'left' of our position.
    while (curr_block > 0 && !(data[curr_block] & curr_rightmost_mask)) {
      curr_block = (curr_block - 1) >> block_shift; // go to parent
      curr_bit_in_block = curr_block_in_level & block_modulo_mask;
      curr_block_in_level >>= block_shift;
      curr_rightmost_mask = ones_from_left_to_index_ex(curr_bit_in_block);
    }

    // Was there anyone to the 'left'?
    if (curr_block == 0 && !(data[curr_block] & curr_rightmost_mask))
      return static_cast<size_t>(-1); // No successor found.

    curr_bit_in_block = indexer.rightmost_pos(data[curr_block] & curr_rightmost_mask);

    // Go down till the base level following the 'rightmost' set bit.		
    while (curr_block < offset) {
      curr_block = (curr_block << block_shift) + curr_bit_in_block + 1; // go to child
      curr_block_in_level = curr_block_in_level << block_shift + curr_bit_in_block;
      curr_bit_in_block = indexer.rightmost_pos(data[curr_block]);
    }

    return ((curr_block - offset) << block_shift) + curr_bit_in_block;
  }
};